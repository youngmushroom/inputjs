import { InputJS } from "../src/Input";

describe("Input Text Box", function () {
    beforeEach(function () {
        var container = document.createElement("div");
        this.input = new InputJS.NumberInput(container);
        document.body.appendChild(container);
    });
    it("Input default show", function () {
        let input = this.input as InputJS.NumberInput;
        expect(input).not.toBeNull();
    });
    it("Empty Diaplay text", function () {
        let input = this.input as InputJS.NumberInput;
        input.setEmptyDisplayFormat("\\$\\ 1234\\.56");
        expect(input.emptyValue()).toBeUndefined();
        expect(input.getEmptyDisplayText()).toEqual("");
        input.emptyValue(9876543);
        expect(input.emptyValue()).not.toBeUndefined();
        expect(input.getEmptyDisplayText()).toEqual("$ 9876.54");
    });
    it("Editing Diaplay text", function () {
        let input = this.input as InputJS.NumberInput;
        input.setEmptyDisplayFormat("\\$\\ 1234\\.56");
        expect(input.emptyValue()).toBeUndefined();
        expect(input.getEmptyDisplayText()).toEqual("");
        input.emptyValue(9876543);
        expect(input.emptyValue()).not.toBeUndefined();
        expect(input.getEmptyDisplayText()).toEqual("$ 9876.54");
    });
    it("Diaplay text", function () {
        let input = this.input as InputJS.NumberInput;
        input.setDisplayFormat("\\$  \\.  ");
        input.value(12345);
        expect(input.value()).toEqual(12345);
        expect(input.getDisplayText()).toEqual("$12.34");
        input.setDisplayFormat("\\$#\\.##");
        input.value(12345);
        expect(input.value()).toEqual(12345);
        expect(input.getDisplayText()).toEqual("$1.23");
    });
    it("value is not null", function () {
        let input = this.input as InputJS.NumberInput;
        input.setDisplayFormat("\\$  \\.  ");
        input.setEditingDisplayFormat("\\%\\ 1234\\.56");
        input.setEmptyDisplayFormat("\\&\\ 1234\\.56");
        input.value(123456);
    });
});
