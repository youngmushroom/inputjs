enum NumberStyles {
    Number,
    Currency,
    Percent,
    Scientific,
    Integer,
}
export class NumberFormat {
    private _maximumIntegerDigits: number;
    private _minimumIntegerDigits: number = 1;
    private _maximumFractionDigits: number = 3;
    private _minimumFractionDigits: number = 0;
    private _minFractionDigits: number = 0;
    private _maxFractionDigits: number = 3;
    private _maxIntegerDigits: number = 40;
    private _minIntegerDigits: number = 1;
    private _groupingUsed: boolean = false;
    private _style: NumberStyles;
    format(num: number) {
        return '';
    }

    public set maximumIntegerDigits(digits: number) {
        this._maximumIntegerDigits = digits;
    }
    public get maximumIntegerDigits() {
        return this._maximumIntegerDigits;
    }
    public set minimumIntegerDigits(digits: number) {
        this._minimumIntegerDigits = digits;
    }
    public get minimumIntegerDigits() {
        return this._minimumIntegerDigits;
    }
    public set maximumFractionDigits(digits: number) {
        this._maximumFractionDigits = digits;
    }
    public get maximumFractionDigits() {
        return this._maximumFractionDigits;
    }

    public set minimumFractionDigits(digits: number) {
        this._minimumFractionDigits = digits;
    }
    public get minimumFractionDigits() {
        return this._minimumFractionDigits;
    }

    public set minFractionDigits(digits: number) {
        this._minFractionDigits = digits;
    }
    public get minFractionDigits() {
        return this._minFractionDigits;
    }
    public set maxFractionDigits(digits: number) {
        this._maxFractionDigits = digits;
    }
    public get maxFractionDigits() {
        return this._maxFractionDigits;
    }
    public set maxIntegerDigits(digits: number) {
        this._maxIntegerDigits = digits;
    }
    public get maxIntegerDigits() {
        return this._maxIntegerDigits;
    }
    public set minIntegerDigits(digits: number) {
        this._minIntegerDigits = digits;
    }
    public get minIntegerDigits() {
        return this._minIntegerDigits;
    }
    public set groupingUsed(groupingUsed: boolean) {
        this._groupingUsed = groupingUsed;
    }
    public get groupingUsed() {
        return this._groupingUsed;
    }
    public set style(style: NumberStyles) {
        this._style = style;
    }
    public get style() {
        return this._style;
    }

}
class DecimalSymbol {
    private _zeroDigit: string;
    private groupingSeparator: string;
    private decimalSeparator: string;
    private perMill: string;
    private percent: string;
    private patternSeparator: string;
}
export class DecimalFormat extends NumberFormat {
    private zeroDigit: string = '0';
    private groupingSeparator: string = ',';
    private decimalSeparator: string = '.';
    private perMille: string = '\u2030';
    private percent: string = '%';
    private digit: string = '#';
    private separator: string = ';';
    private exponent: string = "E";
    private minus: string = '-';
    private currencySign: string = '\u00A4';
    private quote: string = '\'';
    private double_integer_digits: number = 309;
    private double_fraction_digits: number = 340;
    private maximum_integer_digits: number = Number.MAX_SAFE_INTEGER;
    private maximum_fraction_digits: number = Number.MAX_SAFE_INTEGER;
    private positivePrefix: string = "";
    private positiveSuffix: string = "";
    private negativePrefix: string = "-";
    private negativeSuffix: string = "";
    private posPrefixPattern: string;
    private posSuffixPattern: string;
    private negPrefixPattern: string;
    private negSuffixPattern: string;
    private multiplier: number = 1;
    private groupingSize: number = 3;  // invariant, > 0 if useThousands
    private decimalSeparatorAlwaysShown: boolean = false;
    private parseBigDecimal: boolean = false;
    private isCurrencyFormat: boolean = false;
    private multiplier: number = 1;
    private _pattern: string;
    public set pattern(pattern: string) {
        this._pattern = pattern;
    }
    public get pattern() {
        return this._pattern;
    }
    public format(num: number) {
        let isNegative = ((num < 0.0) || (num == 0.0 && 1 / num < 0.0)) !== (this.multiplier < 0);
        return '';
    }
}