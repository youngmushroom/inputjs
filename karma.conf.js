// Karma configuration
// Generated on Wed Dec 05 2018 09:37:48 GMT-0500 (Eastern Standard Time)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', "karma-typescript"],


    // list of files / patterns to load in the browser
    files: [
      './src/**/*.ts',
      './test/**/*Test.spec.ts'
    ],


    // list of files / patterns to exclude
    exclude: [
      './src/**/*.d.ts',
      './test/**/*.d.ts',
      "./dest/**/*.d.ts"
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      "**/*.ts": ["karma-typescript"]
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', "karma-typescript"],
    karmaTypescriptConfig: {
      sourceMap: true,
      tsconfig: "./tsconfig.json"
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    // https://developers.google.com/web/updates/2017/06/headless-karma-mocha-chai
    browsers: ['MyHeadlessChrome', /*"FirefoxHeadless", 'Chrome'*/],
    customLaunchers: {
      MyHeadlessChrome: {
        base: 'ChromeHeadless',
        flags: ['--remote-debugging-port=9223']
      },
      FirefoxHeadless: {
        base: "Firefox",
        flags: [
          "-headless"
        ]
      }
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
