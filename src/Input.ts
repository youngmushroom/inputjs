import { Formatter } from "./Formatter";
export namespace InputJS {
    export type BeforeShow = () => boolean;
    export type AfterShow = () => void;
    export type BeforeHide = () => boolean;
    export type AfterHide = () => void;
    abstract class Component {
        protected _container: HTMLElement;
        private _beforeShow: BeforeShow;
        private _afterShow: AfterShow;
        private _beforeHide: BeforeHide;
        private _afterHide: AfterHide;
        protected _onKeyDown: (e: KeyboardEvent) => void;
        protected _beforeKeydown: (e: KeyboardEvent) => boolean;
        protected _afterKeydown: (e: KeyboardEvent) => void;
        protected _onKeyUp: (e: KeyboardEvent) => void;
        protected _beforeKeyup: (e: KeyboardEvent) => boolean;
        protected _afterKeyup: (e: KeyboardEvent) => void;
        protected _onMouseOver: (e: MouseEvent) => void;
        protected _onValueChanged: (value) => void;
        protected _onValueChanging: (value) => boolean;
        constructor(container: HTMLElement) {
            this._container = container;
        }
        visible(visible: boolean = true): void {
            if (!this.container) {
                return;
            }
            if (visible) {
                this.container.style.visibility = "visible";
            } else {
                this.container.style.visibility = "hidden";
            }
        }

        show(): void {
            let isShow = true;
            if (this._beforeShow) {
                isShow = this._beforeShow.call(this);
            }
            if (isShow) {
                if (this.container) {
                    this.container.style.display = "";
                }
            }
            if (this._afterShow) {
                this._afterShow.call(this);
            }
        }

        beforeShow(callback: BeforeShow): void {
            this._beforeShow = callback;
        }

        afterShow(callback: AfterShow): void {
            this._afterShow = callback;
        }

        hide(): void {
            let isHide = true;
            if (this._beforeHide) {
                isHide = this._beforeHide.call(this);
            }
            if (isHide) {
                if (this.container) {
                    this.container.style.display = "none";
                }
            }
            if (this._afterHide) {
                this._afterHide.call(this);
            }
        }

        beforeHide(callback: BeforeHide): void {
            this._beforeHide = callback;
        }

        afterHide(callback: AfterHide): void {
            this.afterHide = callback;
        }
        css(selector: string, css: CSSStyleSheet): Component {
            return this;
        }

        enable(enable: boolean = true): boolean {
            return false;
        }

        querySelectors(selector: string): NodeListOf<HTMLElement> {
            if (!this.container) {
                return;
            }
            return this.container.querySelectorAll(selector) as NodeListOf<HTMLElement>;
        }
        public beforeKeyDown(callback: (e: KeyboardEvent) => boolean) {
            this._beforeKeydown = callback;
        }
        protected abstract onKeydown(e: KeyboardEvent): void;
        public afterKeyDown(callback: (e: KeyboardEvent) => void) {

        }
        public onValueChanged(callback: (value) => void): void {
            this._onValueChanged = callback;
        }
        public onValueChanging(callback: (value) => boolean): void {
            this._onValueChanging = callback;
        }
        public beforeKeyUp(callback: (e: KeyboardEvent) => boolean) {

        }

        protected abstract onKeyup(e: KeyboardEvent): void;
        public afterKeyUp(callback: (e: KeyboardEvent) => void) {
        }
        protected abstract onMouseOver(e: MouseEvent): void;

        set container(container: HTMLElement) {
            this._container = container;
        }
        get container() {
            return this._container;
        }
    }
    interface InputOption {
        displayFormat: string;
        emptyDisplayFormat: string;
        editingDisplayFormat: string;
        hoverTipFormat: string;
        placeholder: string;
    }

    abstract class AbstractInput extends Component {
        private _option: InputOption;
        constructor(container: HTMLElement) {
            super(container);
            this._option = {} as InputOption;
        }
        public setDisplayFormat(format: string): void {
            this._option.displayFormat = format;
        }
        public setEditingDisplayFormat(format: string): void {
            this._option.editingDisplayFormat = format;
        }
        public setEmptyDisplayFormat(format: string): void {
            this._option.emptyDisplayFormat = format;
        }
        public getEmptyDisplayText(): string {
            return "";
        }
        public getDisplayText(): string {
            return "";
        }
        public getEditingDisplayText(): string {
            return "";
        }
        public setPlaceholder(placeholder: string): void {
            this._option.placeholder = placeholder;
        }
        public setHoverTipFormat(format: string): void {
            this._option.hoverTipFormat = format;
        }
        public isShowTip(): void {

        }
    }
    export class NumberInput extends AbstractInput {
        private nums: number[] = [];
        private _dispalyFormater: Formatter.NumberFormatter;
        private _emptyDisplayFormater: Formatter.NumberFormatter;
        private _editingDisplayFormater: Formatter.NumberFormatter;

        private _input: HTMLInputElement;
        private _displayValue: number | string;
        private _emptyDisplayValue: number | string;
        private _editingDisplayValue: number | string;
        constructor(container: HTMLElement) {
            super(container);
            if (container) {
                this.input = document.createElement("input");
                container.appendChild(this.input);
                this.attachEvent();
            }
        }
        private attachEvent() {
            let onKeydown = (e: KeyboardEvent, value: string) => {
                this.onKeydown(e);
            }
            let onKeyup = (e: KeyboardEvent, value: string) => {
                this.onKeyup(e);
            }
            let onMouseOver = (e: MouseEvent, value: string) => {
                this.onMouseOver(e);
            }
            let onFocus = (e) => {
                this.onFocus(e);
            }
            let onFocusout = (e) => {
                this.onFocusout(e);
            }
            this.input.addEventListener("keydown", function (e: KeyboardEvent) {
                onKeydown(e, this.value);
            });
            this.input.addEventListener("keyup", function (e: KeyboardEvent) {
                onKeyup(e, this.value);
            });
            this.input.addEventListener("mouseover", function (e: MouseEvent) {
                onMouseOver(e, this.value);
            });
            this.input.addEventListener("focus", function (e: MouseEvent) {
                onFocus(e);
            });
            this.input.addEventListener("focusout", function (e: MouseEvent) {
                onFocusout(e);
            });
        }
        public setEditingDisplayFormat(format: string) {
            this._editingDisplayFormater = new Formatter.NumberFormatter(format);
        }
        public setDisplayFormat(format: string) {
            this._dispalyFormater = new Formatter.NumberFormatter(format);
        }
        public setEmptyDisplayFormat(format: string) {
            this._emptyDisplayFormater = new Formatter.NumberFormatter(format);
        }
        public value(value?: string | number): number | string {
            if (undefined === value) {
                return this._displayValue;
            }
            if (["string", "number"].indexOf(typeof value) == -1) {
                throw `Expect 'string | number', but '${typeof value}'`
            }
            if (isNaN(new Number(value).valueOf())) {
                throw `${value} is not a number`;
            }
            let isValid = this._onValueChanging ? this._onValueChanging.call(this, value) : true;
            if (!isValid) {
                return;
            }
            this._displayValue = value;
            if (this._onValueChanged) {
                this._onValueChanged.call(this, this._displayValue);
            }
            this.updateDispalyText(this.getDisplayText());
            return this._displayValue;
        }
        private updateDispalyText(text: string) {
            this.input.value = text;
        }
        private updateEmptyDispalyText(text: string) {
            this.input.value = text;
        }
        private updateEditingDispalyText(text: string) {
            this.input.value = text;
        }
        public getDisplayText() {
            if (this._dispalyFormater) {
                return this._dispalyFormater.format(this.value()) + "";
            }
            if (undefined == this.value()) {
                return "";
            }
            return this._displayValue + "";
        }
        public getEditingDisplayText() {
            if (this._editingDisplayFormater) {
                return this._editingDisplayFormater.format(this.value());
            }
            return this.getDisplayText();
        }
        public emptyValue(value?: string | number) {
            if (undefined !== value) {
                return this._emptyDisplayValue = value;
            }
            return this._emptyDisplayValue;
        }
        public editingValue(value?) {
            if (undefined !== value) {
                this._editingDisplayValue = value;
            }
            return this._editingDisplayValue;
        }
        public getEmptyDisplayText() {
            if (this._emptyDisplayFormater) {
                return this._emptyDisplayFormater.format(this.emptyValue());
            }
            return "";
        }
        protected onKeydown(e: KeyboardEvent): void {
            console.debug(e.keyCode);
            switch (e.keyCode) {
                case KeyCode.Backspace:
                    this.backspaceNumber(this.input.selectionStart, this.input.selectionEnd);
                    break;
                case KeyCode.Enter:
                    break;
                case KeyCode.Delete:
                    this.deleteNmber();
                    break;
                case KeyCode.Left:
                    break;
                case KeyCode.Right:
                    break;
                case KeyCode.Home:
                    break;
                case KeyCode.End:
                    break;
            }
            let isValid = e.keyCode >= KeyCode.Number0 && e.keyCode <= KeyCode.Number9;
            if (this._beforeKeydown) {
                isValid = this._beforeKeydown.call(this, e);
            }
            if (this._afterKeydown) {
                this._afterKeydown.call(this, e);
            }
            if (isValid) {
                this.nums.push(e.keyCode - KeyCode.Number0);
            } else {
                e.stopPropagation();
                e.preventDefault();
            }
        }
        private backspaceNumber(start: number, end: number) {

        }
        private deleteNmber() {

        }

        protected onKeyup(e: KeyboardEvent): void {
            switch (e.keyCode) {
                case KeyCode.Backspace:
                    break;
                case KeyCode.Enter:
                    break;
                case KeyCode.Delete:
                    break;
                case KeyCode.Left:
                    break;
                case KeyCode.Right:
                    break;
                case KeyCode.Home:
                    break;
                case KeyCode.End:
                    break;
            }
            if (this._beforeKeyup) {
                this._beforeKeyup.call(this, e);
            }
            if (this._afterKeyup) {
                this._afterKeyup.call(this, e);
            }
        }

        protected onMouseOver(e: MouseEvent): void {
            //throw new Error("Method not implemented.");
        }
        protected onFocus(e: MouseEvent): void {
            this.updateEditingDispalyText(this.getEditingDisplayText());
        }
        protected onFocusout(e: MouseEvent): void {
            this.updateDispalyText(this.getDisplayText());
        }
        get input(): HTMLInputElement {
            return this._input;
        }
        set input(input: HTMLInputElement) {
            this._input = input;
        }
    }
    enum KeyCode {
        Backspace = 8,
        Enter = 13,
        Shift = 16,
        Ctrl = 17,
        Alt = 18,
        Esc = 27,
        PgUp = 33,
        PgDn = 34,
        End = 35,
        Home = 36,
        Left = 37,
        Up = 38,
        Right = 39,
        Down = 40,
        Delete = 46,
        Number0 = 48,
        Number1 = 49,
        Number2 = 50,
        Number3 = 51,
        Number4 = 52,
        Number5 = 53,
        Number6 = 54,
        Number7 = 55,
        Number8 = 56,
        Number9 = 57,
    }
};