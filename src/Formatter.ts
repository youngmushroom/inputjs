
export module Formatter {

    export interface Formatter<T> {
        format(value: any): T;
    }
    export class TextFormatter implements Formatter<string> {
        format(value: any): string {
            throw new Error("Method not implemented.");
        }
    }
    export class NumberFormatter implements Formatter<string> {
        private formatExpression: FormaterExpression;
        constructor(format) {
            this.formatExpression = new FormaterExpression(format);
        }
        format(value: string | number): string {
            if (undefined == value) {
                return "";
            }
            let describes = this.formatExpression.getExpressionDescsibes();
            if (!describes) {
                return `${value || ""}`;
            }
            let values = `${value}`.split("");
            let index = 0;
            return describes.map((describe) => {
                if (index >= values.length || !describe) {
                    return "";
                }
                if (FormaterExpression.ExpresssionDescribeSymbol != describe.value) {
                    return describe.describe;
                }
                return values[index++];
            }).join("");
        }
    }
    interface ExpresssionDescribe {
        describe: string;
        value: string | number | symbol | {};
    }
    export class FormaterExpression {
        static ExpresssionDescribeSymbol: Symbol | {} = Symbol ? Symbol("FormaterExpression") : Object.create({});
        private describes: ExpresssionDescribe[] = [];
        constructor(format: string) {
            this.analyzeFormat(format);
        }
        private analyzeFormat(formater: string) {
            if ("string" !== typeof formater) {
                throw `Expect 'string', but '${typeof formater}'`;
            }
            let formaters = formater.split('');
            for (let index = 0; index < formaters.length; index++) {
                const element = formaters[index];
                if ("\\" !== element) {
                    this.describes.push({
                        describe: element,
                        value: FormaterExpression.ExpresssionDescribeSymbol,
                    });
                    continue;
                }
                if (index == formater.length - 1) {
                    this.describes.push({
                        describe: "\\",
                        value: "\\"
                    });
                    return;
                }
                index++;
                // skip next
                if (undefined != formaters[index]) {
                    this.describes.push(
                        {
                            describe: formaters[index],
                            value: formaters[index],
                        });
                }
            }

        }
        public getExpressionDescsibes() {
            return this.describes;
        }

    }

}